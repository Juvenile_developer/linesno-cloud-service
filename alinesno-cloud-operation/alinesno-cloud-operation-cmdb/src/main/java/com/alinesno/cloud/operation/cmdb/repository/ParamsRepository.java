package com.alinesno.cloud.operation.cmdb.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.alinesno.cloud.operation.cmdb.entity.ParamsEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface ParamsRepository extends BaseJpaRepository<ParamsEntity, String> {

	/**
	 * 查询参数
	 * @param name
	 * @return
	 */
	@Query("from ParamsEntity t1 where t1.paramName=?1")
	List<ParamsEntity> findByParamName(@Param("name") String name);

	List<ParamsEntity> findByParamNameAndMasterCode(String name, String masterCode);

	
}