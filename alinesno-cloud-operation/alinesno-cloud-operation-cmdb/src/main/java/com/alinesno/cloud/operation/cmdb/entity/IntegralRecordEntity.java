package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 积分记录
 * @author LuoAnDong
 * @since 2018年10月20日 下午8:19:38
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_integral_record")
public class IntegralRecordEntity extends BaseEntity {

	private String orderId ; //订单号
	private String userId ; //用户id
	private int scoreValue ; //积分值 
	private int scoreTotal ; //总积分值
	private String actionCode ; //操作
	private String actionName ; //操作
	private String actionRemark ; //操作备注
	
	public String getActionCode() {
		return actionCode;
	}
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getScoreValue() {
		return scoreValue;
	}
	public void setScoreValue(int scoreValue) {
		this.scoreValue = scoreValue;
	}
	public int getScoreTotal() {
		return scoreTotal;
	}
	public void setScoreTotal(int scoreTotal) {
		this.scoreTotal = scoreTotal;
	}
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public String getActionRemark() {
		return actionRemark;
	}
	public void setActionRemark(String actionRemark) {
		this.actionRemark = actionRemark;
	}
	
}
