package com.alinesno.cloud.operation.cmdb.service;

import com.alinesno.cloud.operation.cmdb.entity.WechatMessageEntity;

public interface WechatMessageService {

	/**
	 * 保存用户
	 * @param wechatMessageEntity
	 */
	void save(WechatMessageEntity wechatMessageEntity);

}
