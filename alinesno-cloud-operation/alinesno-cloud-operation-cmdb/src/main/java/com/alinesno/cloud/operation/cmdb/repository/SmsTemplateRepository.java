package com.alinesno.cloud.operation.cmdb.repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.alinesno.cloud.operation.cmdb.entity.SmsTemplateEntity;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface SmsTemplateRepository extends BaseJpaRepository<SmsTemplateEntity, String> {

	/**
	 * 通过模板id查询模板
	 * @param template
	 * @return
	 */
	@Query("from SmsTemplateEntity t1 where t1.templateId=?1")
	SmsTemplateEntity findAllByTemplate(@Param("template") String template);

}