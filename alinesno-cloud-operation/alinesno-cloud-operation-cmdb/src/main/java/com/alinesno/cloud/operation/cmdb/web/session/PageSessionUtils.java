package com.alinesno.cloud.operation.cmdb.web.session;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alinesno.cloud.operation.cmdb.common.constants.RunConstants;
import com.alinesno.cloud.operation.cmdb.web.bean.CurrentUserBean;


/**
 * 页面session配置 
 * @author LuoAnDong
 * @since 2018年9月23日 下午9:27:45
 */
@Component
public class PageSessionUtils {
	
	private Logger logger = LoggerFactory.getLogger(PageSessionUtils.class) ; 

	@Value("${wechat.dev.model}")
	private boolean wechatDev ; 
	
	@Value("${wechat.dev.user.school}")
	private String devMasterCode ;  //开发机房ID
	
	@Value("${wechat.dev.user.openId}")
	private String devOpenId ; // 开发用户
	
	@Value("${wechat.dev.user.id}")
	private String devUserId ; //用户id
	
	@Value("${wechat.dev.user.phone}")
	private String devUserPhone ; //用户手机号
	
	@Value("${wechat.dev.user.prop}")
	private String devUserProp ; //用户手机号
	
	public CurrentUserBean pageSession(HttpServletRequest request) {
		Object currentUser = request.getSession().getAttribute(RunConstants.CURRENT_USER) ; 
		
		if(currentUser != null) {
			CurrentUserBean bean = (CurrentUserBean) currentUser ; 
			
			logger.debug("当前用户:{}" , ToStringBuilder.reflectionToString(bean));
			return bean ; 
		}
	
		logger.debug("是否为开发者模式:{}" , wechatDev);
		if(wechatDev) {
			CurrentUserBean u = new CurrentUserBean(devUserId, "微信本地测试用户", devUserPhone , devMasterCode, devOpenId) ; 
			u.setFieldProp(devUserProp);
			return u ; 
		}
		return null ; 
	}
	
}
