package com.alinesno.cloud.operation.cmdb.service.impl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.operation.cmdb.common.constants.ParamsEnum;
import com.alinesno.cloud.operation.cmdb.common.constants.RunerEnum;
import com.alinesno.cloud.operation.cmdb.common.util.DateUtil;
import com.alinesno.cloud.operation.cmdb.entity.ParamsEntity;
import com.alinesno.cloud.operation.cmdb.entity.ServerEntity;
import com.alinesno.cloud.operation.cmdb.repository.BudinessServerRepository;
import com.alinesno.cloud.operation.cmdb.repository.ParamsRepository;
import com.alinesno.cloud.operation.cmdb.service.ParamsService;

/**
 * 参数服务类
 * 
 * @author LuoAnDong
 * @since 2018年8月13日 上午7:20:19
 */
@Service
public class ParamsServiceImpl implements ParamsService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	// 超时，按秒
	@Value("${application.order.out-time}")
	public int outTimes;

	@Autowired
	private ParamsRepository paramsRepository;
	
	@Autowired
	private BudinessServerRepository budinessServerRepository ; 

	@Override
	public ParamsEntity findParamByName(String name , String masterCode) {
		logger.debug("参数名称:{} , masterCode = {}", name , masterCode);
		List<ParamsEntity> ps = null ; 
		if(StringUtils.isNoneBlank(masterCode)) {
			ps = paramsRepository.findByParamNameAndMasterCode(name , masterCode);
		}else {
			ps = paramsRepository.findByParamName(name);
		}
		return (ps == null || ps.isEmpty()) ? null : ps.get(0);
	}

	/**
	 * 添加参数
	 * 
	 * @param params
	 * @return
	 */
	@Override
	public ParamsEntity addParams(ParamsEntity params) {
		params.setAddTime(new Timestamp(System.currentTimeMillis()));
		return paramsRepository.save(params);
	}

	@Override
	public Iterable<ParamsEntity> addParamsList(List<ParamsEntity> ps) {
		return paramsRepository.saveAll(ps);
	}

	@Override
	public int findParamByNameToInt(String name , String masterCode) {
		ParamsEntity p = this.findParamByName(name , masterCode);
		if (p != null && StringUtils.isNoneBlank(p.getParamValue())) {
			return Integer.parseInt(p.getParamValue());
		}
		return 0;
	}

	@Override
	public boolean isRunningTime(String masterCode) {
		return this.isBewteen(ParamsEnum.RUNNING_TIME.getCode() , masterCode);
	}

	@Override
	public boolean isShopRunningTime(String masterCode) {
		return this.isBewteen(ParamsEnum.RUNNING_SHOP_TIME.getCode() , masterCode);
	}

	private boolean isBewteen(String param , String masterCode) {
		logger.debug("param = {} , masterCode = {}" , param , masterCode);
		ParamsEntity p = findParamByName(param , masterCode);

		if (p != null && StringUtils.isNoneBlank(p.getParamValue()) && p.getParamValue().contains("-")) {

			String v = p.getParamValue();
			String[] arr = v.split("-");
			String startTime = arr[0];
			String endTime = arr[1];
			logger.debug("超市运营时间判断,开始时间:{} , 结束时间:{}", startTime, endTime);

			if (StringUtils.isNoneBlank(startTime) && StringUtils.isNoneBlank(endTime)) {
				try {
					boolean b = DateUtil.isBelong(startTime, endTime);
					return b;
				} catch (ParseException e) {
					logger.debug("时间格式化出错:{}", e);
				}
			}

		}

		return false;
	}

	@Override
	public void initSchoolParam(String masterCode) {
		
		//------- 初始化服务_start ---------------------------------------------
		ServerEntity e1 = new ServerEntity() ; 
		e1.setServerName("172.20.168.1");
		e1.setServerCode("1");
		e1.setServerDesc("持续集成服务器");
		e1.setAddTime(new Date());
		e1.setFieldProp("使用中");
		e1.setHasStatus(0);
		e1.setServerOrder(4);
		e1.setMasterCode(masterCode);
		
		ServerEntity e2 = new ServerEntity() ; 
		e2.setServerName("172.20.174.12");
		e2.setServerCode("2");
		e2.setServerDesc("业务服务服务器");
		e2.setAddTime(new Date());;
		e2.setFieldProp("使用中");
		e2.setHasStatus(0);
		e2.setServerOrder(3);
		e2.setMasterCode(masterCode);
		
		ServerEntity e3 = new ServerEntity() ; 
		e3.setServerName("192.168.1.123");
		e3.setServerCode("3");
		e3.setServerDesc("前端应用服务器");
		e3.setAddTime(new Date());;
		e3.setFieldProp("使用中");
		e3.setHasStatus(0);
		e3.setServerOrder(2);
		e3.setMasterCode(masterCode);
		
		ServerEntity e4 = new ServerEntity() ; 
		e4.setServerName("192.168.2.124");
		e4.setServerCode("4");
		e4.setServerDesc("业务测试服务器");
		e4.setAddTime(new Date());
		e4.setFieldProp("使用中");
		e4.setHasStatus(0);
		e4.setServerOrder(1);
		e4.setMasterCode(masterCode);
		
		List<ServerEntity> es = new ArrayList<ServerEntity>() ; 
		es.add(e1) ; 
		es.add(e2) ; 
		es.add(e3) ; 
		es.add(e4) ; 
		
		Iterable<ServerEntity> b1 = budinessServerRepository.saveAll(es) ; 
		logger.debug("初始化服务列表:{}" , b1);
		//------- 初始化服务_start ---------------------------------------------

		// 运营时间
		ParamsEntity p1 = new ParamsEntity();
		p1.setParamName(ParamsEnum.RUNNING_TIME.getCode());
		p1.setParamDesc(ParamsEnum.RUNNING_TIME.getText());
		p1.setParamValue("06:12-23:59");
		p1.setMasterCode(masterCode);

		ParamsEntity p2 = new ParamsEntity();
		p2.setParamName(ParamsEnum.ORDER_OUTTIME.getCode()); // "order_outtime");
		p2.setParamDesc(ParamsEnum.ORDER_OUTTIME.getText());
		p2.setParamValue("900");
		p2.setMasterCode(masterCode);

		ParamsEntity p3 = new ParamsEntity();
		p3.setParamName(ParamsEnum.ORDER_SALA_OUTTIME.getCode()); // "order_sala_outtime");
		p3.setParamDesc(ParamsEnum.ORDER_SALA_OUTTIME.getText());
		p3.setParamValue("60");
		p3.setMasterCode(masterCode);

		ParamsEntity p4 = new ParamsEntity();
		p4.setParamName(ParamsEnum.ORDER_MIN_PAY.getCode()); // "order_min_pay");
		p4.setParamDesc(ParamsEnum.ORDER_MIN_PAY.getText());
		p4.setParamValue("2");
		p4.setMasterCode(masterCode);

		ParamsEntity p5 = new ParamsEntity();
		p5.setParamName(ParamsEnum.RUNNING_SHOP_TIME.getCode()); // "running_shop_time");
		p5.setParamDesc(ParamsEnum.RUNNING_SHOP_TIME.getText());
		p5.setParamValue("06:12-23:59");
		p5.setMasterCode(masterCode);

		ParamsEntity p6 = new ParamsEntity();
		p6.setParamName(ParamsEnum.RUNNING_SHOP_MIN_PAY.getCode()); // "running_shop_min_pay");
		p6.setParamDesc(ParamsEnum.RUNNING_SHOP_MIN_PAY.getText());
		p6.setParamValue("10");
		p6.setMasterCode(masterCode);
		
		ParamsEntity p7 = new ParamsEntity();
		p7.setParamName(ParamsEnum.USER_APPLY_TYPE.getCode());
		p7.setParamDesc(ParamsEnum.USER_APPLY_TYPE.getText());
		p7.setParamValue(RunerEnum.USER_APPLY_TYPE_SIMPLE.getCode());
		p7.setMasterCode(masterCode);	
		
		ParamsEntity p8 = new ParamsEntity();
		p8.setParamName(ParamsEnum.USER_APPLY_DESC.getCode());
		p8.setParamDesc(ParamsEnum.USER_APPLY_DESC.getText());
		p8.setParamValue("为保障云主机服务和客户体验,请镖师遵守如下规则:\n1. 镖师接单必须积极接单,订单任务及时处理并签收;\n2. 如因其它原因不能及时跑单,及时与客户沟通或给管理员反馈;\n3. 订单完成,镖师需在公众号及时【签收】");
		p8.setMasterCode(masterCode);

		List<ParamsEntity> ps = new ArrayList<ParamsEntity>();
		ps.add(p1);
		ps.add(p2);
		ps.add(p3);
		ps.add(p4);
		ps.add(p5);
		ps.add(p6);
		ps.add(p7);
		ps.add(p8);

		Iterable<ParamsEntity> b = paramsRepository.saveAll(ps);
		logger.debug("初始化机房参数.结果:{}" , b);
	}

}
