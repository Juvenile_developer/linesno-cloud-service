package com.alinesno.cloud.operation.cmdb.web.bean;

/**
 * 监控项目
 * @author LuoAnDong
 * @since 2019年5月9日 下午10:15:34
 */
public class MonitorBean {

	private String id ; 
	private String jobName ; // 监控名称
	private String jobStatus ; // 监控项状态 
	
	public MonitorBean(String jobName) {
		super();
		this.jobName = jobName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
}
