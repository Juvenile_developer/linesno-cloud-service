package com.alinesno.cloud.operation.cmdb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.operation.cmdb.entity.IntegralRecordEntity;
import com.alinesno.cloud.operation.cmdb.repository.IntegralRecordRepository;
import com.alinesno.cloud.operation.cmdb.service.IntegralService;

/**
 * 用户积分服务
 * @author LuoAnDong
 * @since 2018年10月20日 下午9:37:23
 */
@Service
public class IntegralServiceImpl implements IntegralService {

	@Autowired
	private IntegralRecordRepository integralRecordRepository ; 
	
	@Override
	public List<IntegralRecordEntity> findIntegralRecord(String userId) {
		List<IntegralRecordEntity> list = integralRecordRepository.findByUserIdOrderByAddTimeDesc(PageRequest.of(0, 20),userId) ; 
		return list ;
	}

}
