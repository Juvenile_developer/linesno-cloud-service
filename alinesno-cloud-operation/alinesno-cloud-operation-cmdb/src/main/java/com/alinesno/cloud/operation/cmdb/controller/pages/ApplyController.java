package com.alinesno.cloud.operation.cmdb.controller.pages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.operation.cmdb.controller.BaseController;

/**
 * 门户服务 
 * @author LuoAnDong
 * @since 2019年3月19日 下午12:00:32
 */
@Controller
@RequestMapping("/apply")
public class ApplyController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(ApplyController.class) ; 
	
	/**
	 * 主机申请
	 * @param model
	 * @return
	 */
	@GetMapping("/machine")
	public String machine(Model model) {
		logger.debug("主机申请");
		return WX_PAGE+"machine";
	}	
	
	/**
	 * 数据库申请
	 * @param model
	 * @return
	 */
	@GetMapping("/database")
	public String database(Model model) {
		logger.debug("数据库申请");
		return WX_PAGE+"database";
	}	
}
