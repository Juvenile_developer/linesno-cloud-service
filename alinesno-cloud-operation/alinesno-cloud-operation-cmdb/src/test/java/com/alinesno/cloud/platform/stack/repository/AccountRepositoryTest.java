package com.alinesno.cloud.platform.stack.repository;

import java.sql.Timestamp;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.entity.AccountEntity;
import com.alinesno.cloud.operation.cmdb.repository.AccountRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountRepositoryTest {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 
	
	@Autowired
	private AccountRepository accountRepository ; 

	@Test
	public void testSave() {
		AccountEntity e = new AccountEntity() ; 
		
		e.setLoginName("admin");
		e.setSalt("1234");
		e.setPassword("admin");
		e.setAccountStatus("1");
		e.setAddTime(new Timestamp(System.currentTimeMillis()));
		e.setName("超级管理员");
		
		accountRepository.save(e) ; 
	}
	
	@Test
	public void testFindByLoginNameAndSalt() {
		AccountEntity e = accountRepository.findByLoginNameAndSalt("admin", "1234") ; 
		logger.debug("e = {}" , ToStringBuilder.reflectionToString(e));
	}

}
