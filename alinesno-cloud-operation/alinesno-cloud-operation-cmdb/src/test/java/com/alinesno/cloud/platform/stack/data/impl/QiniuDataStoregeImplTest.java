package com.alinesno.cloud.platform.stack.data.impl;

import static org.junit.Assert.fail;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.operation.cmdb.third.data.DataStorege;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QiniuDataStoregeImplTest {

	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 

	@Resource(name="qiniuDataStoregeImpl")
	private DataStorege dataStorege ; 

	@Test
	public void testUploadData() {
		String path = dataStorege.uploadData("/Users/luodong/Downloads/8B2D532F9AA7C05A74312A25DA3D3F9E.png") ; 
		logger.debug("path = {}" , path);
	}

	@Test
	public void testDownloadData() {
		String path = dataStorege.downloadData("20180916071645490783081985212416") ; 
		logger.debug("path = {}", path);
	}

	@Test
	public void testDeleteData() {
		fail("Not yet implemented");
	}

}
