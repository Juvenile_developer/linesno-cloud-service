package com.alinesno.cloud.base.notice.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.base.notice.feign.dto.EmailHistoryDto ;
import com.alinesno.cloud.base.notice.feign.facade.EmailHistoryFeigin ;
import com.alinesno.cloud.common.core.constants.SpringInstanceScope;
import com.alinesno.cloud.common.web.base.advice.TranslateCode;
import com.alinesno.cloud.common.web.base.bean.DatatablesPageBean;
import com.alinesno.cloud.common.web.base.controller.FeignMethodController;

/**
 * <p> 前端控制器 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 06:27:10
 */
@Controller
@Scope(SpringInstanceScope.PROTOTYPE)
@RequestMapping("base/notice/emailHistory")
public class EmailHistoryController extends FeignMethodController<EmailHistoryDto, EmailHistoryFeigin> {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(EmailHistoryController.class);

	@TranslateCode(value="[{hasStatus:has_status}]")
	@ResponseBody
	@PostMapping("/datatables")
    public DatatablesPageBean datatables(HttpServletRequest request , Model model ,  DatatablesPageBean page){
		log.debug("page = {}" , ToStringBuilder.reflectionToString(page));
		return this.toPage(model, feign , page) ;
    }


}


























