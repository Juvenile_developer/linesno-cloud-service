package com.alinesno.cloud.compoment.kafka.enable;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

import com.alinesno.cloud.compoment.kafka.config.KafkaProducerConfig;
import com.alinesno.cloud.compoment.kafka.impl.KafkaComponentServiceImpl;

/**
 * 引入自动类
 * 
 * @author LuoAnDong
 * @sine 2019年4月5日 下午3:34:07
 */
public class ProducerConfigurationSelector implements ImportSelector {

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {
		List<String> importBean = new ArrayList<String>();

		importBean.add(KafkaProducerConfig.class.getName());
		importBean.add(KafkaComponentServiceImpl.class.getName());

		return importBean.toArray(new String[] {});
	}

}
