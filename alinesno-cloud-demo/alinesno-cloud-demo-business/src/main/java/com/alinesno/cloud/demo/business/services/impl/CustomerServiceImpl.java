package com.alinesno.cloud.demo.business.services.impl;

import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.demo.business.entity.CustomerEntity;
import com.alinesno.cloud.demo.business.repository.CustomerRepository;
import com.alinesno.cloud.demo.business.services.ICustomerService;

@Service
public class CustomerServiceImpl extends IBaseServiceImpl<CustomerRepository, CustomerEntity, String> implements ICustomerService {


}
