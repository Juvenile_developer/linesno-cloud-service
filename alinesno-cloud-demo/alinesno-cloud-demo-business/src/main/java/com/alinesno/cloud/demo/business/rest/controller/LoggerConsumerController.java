package com.alinesno.cloud.demo.business.rest.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.common.facade.pageable.RestPage;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;
import com.alinesno.cloud.demo.base.feign.dto.LoggerDto;
import com.alinesno.cloud.demo.base.feign.facade.ILoggerFeign;

/**
 * 日志操作控制层(用于日志Feign接口测试)
 * 
 * @author LuoAnDong
 * @since 2018年12月12日 下午12:53:19
 */
@RestController
@RequestMapping("customer")
public class LoggerConsumerController {

	private static final Logger log = LoggerFactory.getLogger(LoggerConsumerController.class);

	@Autowired
	private ILoggerFeign loggerFeign;

	@GetMapping("findAllById")
	public List<LoggerDto> findAllById() {
		ArrayList<String> ids = new ArrayList<String>();
		log.debug("ids = {}", Arrays.asList(ids));

		ids.add("521424876708298752");

		return loggerFeign.findAllById(ids);
	}

	@GetMapping("saveAll")
	public List<LoggerDto> saveall() {

		// TODO feign超时的问题
		List<LoggerDto> entities = new ArrayList<LoggerDto>();

		for (int i = 0; i < 100; i++) {
			LoggerDto entity = new LoggerDto();
			entity.setRecordChannel("loggs_" + i);
			entity.setRecordMsg("日志集成管道_" + i);
			entity.setRecordParams("params_" + i);
			entity.setRecordUser("zhangsan_" + i);
			entity.setRecordUserName("张三_" + i);

			entities.add(entity);
		}

		return loggerFeign.saveAll(entities);
	}

	@GetMapping("saveAndFlush")
	public LoggerDto saveAndFlush() {
		LoggerDto entity = new LoggerDto();
		entity.setRecordChannel("loggs");
		entity.setRecordMsg("日志集成管道");
		entity.setRecordParams("params");
		entity.setRecordUser("zhangsan");
		entity.setRecordUserName("张三");
		return loggerFeign.saveAndFlush(entity);
	}

	@GetMapping("deleteInBatch")
	public void deleteInBatch() {
		List<LoggerDto> entities = new ArrayList<LoggerDto>();

		for (int i = 0; i < 90; i++) {
			LoggerDto entity = new LoggerDto();
			entity.setRecordChannel("loggs_" + i);
			entity.setRecordMsg("日志集成管道_" + i);
			entity.setRecordParams("params_" + i);
			entity.setRecordUser("zhangsan_" + i);
			entity.setRecordUserName("张三_" + i);

			entities.add(entity);
		}
		loggerFeign.deleteInBatch(entities);
	}

	@GetMapping("deleteAllInBatch")
	public void deleteAllInBatch() {
		loggerFeign.deleteAllInBatch();
	}

	@GetMapping("getOne")
	public LoggerDto getOne() {
		String id = "522550790028525568";
		return loggerFeign.getOne(id);
	}

	@PostMapping("findAllByPageable")
	public RestPage<LoggerDto> findAllByPageable() {
		Pageable pageable = PageRequest.of(1, 5);

		log.debug("pageable = {}", ToStringBuilder.reflectionToString(pageable));

		RestPage<LoggerDto> page = loggerFeign.findAll(new RestPage<LoggerDto>(1,5));

		log.debug("page = {}", ToStringBuilder.reflectionToString(page));

		return page;
	}

	@PostMapping("save")
	public LoggerDto save() {
		int i = 0;
		LoggerDto entity = new LoggerDto();
		entity.setRecordChannel("loggs_" + i);
		entity.setRecordMsg("日志集成管道_" + i);
		entity.setRecordParams("params_" + i);
		entity.setRecordUser("zhangsan_" + i);
		entity.setRecordUserName("张三_" + i);
		return loggerFeign.save(entity);
	}

	@GetMapping("findById")
	public Optional<LoggerDto> findById() {
		String id = "522550790028525568";
		log.debug("findById = {}", id);
		return loggerFeign.findById(id);
	}

	@GetMapping("existsById")
	public boolean existsById() {
		String id = "522550790028525568";
		return loggerFeign.existsById(id);
	}

	@GetMapping("count")
	public long count() {
		return loggerFeign.count();
	}

	@GetMapping("deleteById")
	public void deleteById(String id) {
		loggerFeign.deleteById(id);
	}

	@GetMapping("delete")
	public void delete() {
		String id = "522550790028525568";

		LoggerDto entity = new LoggerDto();
		entity.setId(id);
		entity.setRecordChannel("loggs");
		entity.setRecordMsg("日志集成管道");
		entity.setRecordParams("params");
		entity.setRecordUser("zhangsan");
		entity.setRecordUserName("张三");

		loggerFeign.delete(entity);
	}

	@GetMapping("deleteAllByIterable")
	public void deleteAllByIterable() {
		List<LoggerDto> entities = new ArrayList<LoggerDto>();

		for (int i = 0; i < 90; i++) {
			LoggerDto entity = new LoggerDto();
			entity.setRecordChannel("loggs_" + i);
			entity.setRecordMsg("日志集成管道_" + i);
			entity.setRecordParams("params_" + i);
			entity.setRecordUser("zhangsan_" + i);
			entity.setRecordUserName("张三_" + i);

			entities.add(entity);
		}

		loggerFeign.deleteAll(entities);
	}

	@PostMapping("deleteAll")
	public void deleteAll() {
		loggerFeign.deleteAll();
	}

	@PostMapping("findOneByWrapper")
	public Optional<LoggerDto> findOne() {

		RestWrapper e = new RestWrapper();
		e.eq("recordMsg", "日志集成管道_88");

		return loggerFeign.findOne(e);
	}

	@PostMapping("findAllByWrapper")
	public List<LoggerDto> findAllByWrapper() {

		RestWrapper e = new RestWrapper();
		e.eq("recordMsg", "日志集成管道");

		return loggerFeign.findAll(e);
	}

	@PostMapping("findAllByWrapperAndPageable")
	public Page<LoggerDto> findAllByWrapperAndPageable() {

		RestWrapper e = new RestWrapper();
		e.eq("recordMsg", "日志集成管道_2");

		e.setPageable(new RestPage<LoggerDto>(0, 8));

		// return loggerFeign.findAllByWrapperAndPageable(e);
		
		return null ; 
	}

}
