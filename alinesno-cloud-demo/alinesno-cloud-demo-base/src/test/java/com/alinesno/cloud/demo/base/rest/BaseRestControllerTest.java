package com.alinesno.cloud.demo.base.rest;

import static org.junit.Assert.fail;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import com.alibaba.fastjson.JSONObject;
import com.alinesno.cloud.demo.base.entity.LoggerEntity;

public class BaseRestControllerTest {

	private static final Logger log = LoggerFactory.getLogger(BaseRestControllerTest.class);

	@Test
	public void testFindOneExampleOfE() {
		fail("Not yet implemented");
	}

	@SuppressWarnings({ "serial", "rawtypes" })
	@Test
	public void testFindAllSpecificationOfE() {

		Specification<LoggerEntity> spec = new Specification<LoggerEntity>() {
			@Override
			public Predicate toPredicate(Root<LoggerEntity> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				Predicate predicate = criteriaBuilder.conjunction();
				predicate.getExpressions().add(criteriaBuilder.equal(root.get("activityType"), "张三"));
				return predicate;
			}
		};

		Object json = JSONObject.toJSONString(spec);
		
		RestSpecification rest = new RestSpecification() ; 
		
//		CriteriaBuilder criteriaBuilder = new CriteriaBuilderImpl(); 
//		Predicate predicate = criteriaBuilder.conjunction();
//		predicate.getExpressions().add(criteriaBuilder.equal("activityType"), "张三");
		
		log.debug("json = {} , rest = {}", json , JSONObject.toJSON(rest));
	}

	@SuppressWarnings("serial")
	class RestSpecification<T> implements Specification<T> {

		@Override
		public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
			Predicate predicate = criteriaBuilder.conjunction();
			predicate.getExpressions().add(criteriaBuilder.equal(root.get("activityType"), "张三"));
			return predicate;
		}

	}

}
