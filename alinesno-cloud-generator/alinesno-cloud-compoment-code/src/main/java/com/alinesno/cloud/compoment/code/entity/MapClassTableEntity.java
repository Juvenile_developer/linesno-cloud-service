package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 类表映射信息
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="map_class_table")
public class MapClassTableEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 映射编码
     */
	private String code;
    /**
     * 表名
     */
	private String tableName;
    /**
     * 类名
     */
	private String className;
    /**
     * 注释
     */
	private String notes;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}


	@Override
	public String toString() {
		return "MapClassTableEntity{" +
			"code=" + code +
			", tableName=" + tableName +
			", className=" + className +
			", notes=" + notes +
			"}";
	}
}
