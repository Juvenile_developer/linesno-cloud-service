package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 生成源码资料
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="project_code_catalog")
public class ProjectCodeCatalogEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编码
     */
	private String code;
    /**
     * 项目编码
     */
	private String projectCode;
    /**
     * 项目基础包名
     */
	private String basePackage;
    /**
     * 文件名
     */
	private String fileName;
    /**
     * 文件后缀
     */
	private String fileSuffix;
    /**
     * 相对路径
     */
	private String relativePath;
    /**
     * 绝对路径
     */
	private String absolutePath;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getBasePackage() {
		return basePackage;
	}

	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSuffix() {
		return fileSuffix;
	}

	public void setFileSuffix(String fileSuffix) {
		this.fileSuffix = fileSuffix;
	}

	public String getRelativePath() {
		return relativePath;
	}

	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}


	@Override
	public String toString() {
		return "ProjectCodeCatalogEntity{" +
			"code=" + code +
			", projectCode=" + projectCode +
			", basePackage=" + basePackage +
			", fileName=" + fileName +
			", fileSuffix=" + fileSuffix +
			", relativePath=" + relativePath +
			", absolutePath=" + absolutePath +
			"}";
	}
}
