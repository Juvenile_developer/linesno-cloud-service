package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 项目数据表
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="project_map")
public class ProjectMapEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 项目编码
     */
	private String projectCode;
    /**
     * 字段属性映射编码
     */
	private String mapClassTableCode;


	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}

	public String getMapClassTableCode() {
		return mapClassTableCode;
	}

	public void setMapClassTableCode(String mapClassTableCode) {
		this.mapClassTableCode = mapClassTableCode;
	}


	@Override
	public String toString() {
		return "ProjectMapEntity{" +
			"projectCode=" + projectCode +
			", mapClassTableCode=" + mapClassTableCode +
			"}";
	}
}
