package com.alinesno.cloud.compoment.code.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 设置
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@Entity
@Table(name="setting")
public class SettingEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 键
     */
	private String k;
    /**
     * 值
     */
	private String v;
    /**
     * 说明
     */
	private String description;


	public String getK() {
		return k;
	}

	public void setK(String k) {
		this.k = k;
	}

	public String getV() {
		return v;
	}

	public void setV(String v) {
		this.v = v;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return "SettingEntity{" +
			"k=" + k +
			", v=" + v +
			", description=" + description +
			"}";
	}
}
