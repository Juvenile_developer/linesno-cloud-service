package com.alinesno.cloud.base.boot.service.impl;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.service.IManagerApplicationService;
import com.alinesno.cloud.common.core.junit.JUnitBase;

/**
 * 管理员
 * @author LuoAnDong
 * @since 2019年4月15日 下午10:11:28
 */
public class ManagerApplicationServiceImplTest extends JUnitBase {

	@Autowired
	private IManagerApplicationService managerApplicationService ; 
	
	@Test
	public void testFindAllByAccount() {
		String accountId = "567263477580693504" ; 
		List<ManagerApplicationEntity> as = managerApplicationService.findAllByAccountId(accountId) ; 
	
		for(ManagerApplicationEntity a : as) {
			log.debug("bean:{}" , ToStringBuilder.reflectionToString(a));
		}
		
	}

}
