package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ManagerSourceGenerateEntity;
import com.alinesno.cloud.base.boot.repository.ManagerSourceGenerateRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-17 07:44:37
 */
@NoRepositoryBean
public interface IManagerSourceGenerateService extends IBaseService<ManagerSourceGenerateRepository, ManagerSourceGenerateEntity, String> {

}
