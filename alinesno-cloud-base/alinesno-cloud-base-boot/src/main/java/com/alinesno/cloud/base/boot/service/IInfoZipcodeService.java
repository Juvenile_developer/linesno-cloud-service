package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.InfoZipcodeEntity;
import com.alinesno.cloud.base.boot.repository.InfoZipcodeRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IInfoZipcodeService extends IBaseService<InfoZipcodeRepository, InfoZipcodeEntity, String> {

}
