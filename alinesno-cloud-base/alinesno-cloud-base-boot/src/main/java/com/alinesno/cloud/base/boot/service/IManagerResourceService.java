package com.alinesno.cloud.base.boot.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.base.boot.repository.ManagerResourceRepository;
import com.alinesno.cloud.common.core.services.IBaseService;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@NoRepositoryBean
public interface IManagerResourceService extends IBaseService<ManagerResourceRepository, ManagerResourceEntity, String> {
	
	/**
	 * 通过一级目录构建菜单 
	 * @param resourceParent
	 * @param applicationId 
	 * @return
	 */
	ManagerResourceEntity findMenus(String resourceParent, String applicationId);

	/**
	 * 通过一级目录构建菜单 
	 * @param resourceParent
	 * @param applicationId 
	 * @return
	 */
	ManagerResourceEntity findMenus(String resourceParent, String applicationId, String accountId);

}
