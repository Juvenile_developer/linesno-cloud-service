package com.alinesno.cloud.base.boot.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-02-07 21:16:11
 */
@SuppressWarnings("serial")
public class ManagerCodeTypeDto extends BaseDto {

    /**
     * 代码类型名称
     */
	private String codeTypeName;
	
    /**
     * 代码类型值
     */
	private String codeTypeValue;
	


	public String getCodeTypeName() {
		return codeTypeName;
	}

	public void setCodeTypeName(String codeTypeName) {
		this.codeTypeName = codeTypeName;
	}

	public String getCodeTypeValue() {
		return codeTypeValue;
	}

	public void setCodeTypeValue(String codeTypeValue) {
		this.codeTypeValue = codeTypeValue;
	}

}
