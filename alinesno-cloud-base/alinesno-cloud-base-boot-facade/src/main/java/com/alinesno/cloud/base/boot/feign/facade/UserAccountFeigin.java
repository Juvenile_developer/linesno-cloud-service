package com.alinesno.cloud.base.boot.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;

import com.alinesno.cloud.base.boot.feign.dto.UserAccountDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p> 基础账户表 请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:02:27
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="userAccount")
public interface UserAccountFeigin extends IBaseFeign<UserAccountDto> {

}
