package com.alinesno.cloud.base.message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 启动入口
 * 
 * @author LuoAnDong
 * @since 2018-12-16 18:12:212
 */
@EnableAsync // 开启异步任务
@EnableEurekaClient // 开启eureka
@EnableTransactionManagement // 开启事务
@SpringBootApplication  // 启动入口(必须)
public class AlinesnoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlinesnoApplication.class, args);
	}
}
