package com.alinesno.cloud.base.message.enums;

/**
 * 消息是否死亡状态
 * @author LuoAnDong
 * @since 2018年12月1日 上午11:07:08
 */
public enum MessageDeathEnum {

	/**
	 * 已经死亡(1)
	 */
	HAS_DEAD(1) , 
	
	/**
	 * 未死亡(0)
	 */
	HAS_ALIVE(0) ; 
	
	
	private int value ; 
	
	private MessageDeathEnum(int i){
		value = i ; 
	}
	
	public int value(){
		return value ; 
	}
}
