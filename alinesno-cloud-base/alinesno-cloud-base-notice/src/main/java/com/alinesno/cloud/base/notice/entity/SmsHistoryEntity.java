package com.alinesno.cloud.base.notice.entity;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Entity;
import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 07:19:08
 */
@Entity
@Table(name="sms_history")
public class SmsHistoryEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 业务关键字
     */
	@Column(name="biz_id")
	private String bizId;
    /**
     * 业务主键
     */
	@Column(name="business_id")
	private String businessId;
    /**
     * 短信内容
     */
	private String content;
    /**
     * 其它属性
     */
	@Column(name="out_id")
	private String outId;
    /**
     * 手机号
     */
	private String phone;
    /**
     * 短信签名
     */
	@Column(name="sign_name")
	private String signName;
    /**
     * 短信模板
     */
	private String template;
    /**
     * 模板代码
     */
	@Column(name="template_code")
	private String templateCode;
    /**
     * 验证码
     */
	@Column(name="validate_code")
	private String validateCode;


	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getOutId() {
		return outId;
	}

	public void setOutId(String outId) {
		this.outId = outId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSignName() {
		return signName;
	}

	public void setSignName(String signName) {
		this.signName = signName;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}


	@Override
	public String toString() {
		return "SmsHistoryEntity{" +
			"bizId=" + bizId +
			", businessId=" + businessId +
			", content=" + content +
			", outId=" + outId +
			", phone=" + phone +
			", signName=" + signName +
			", template=" + template +
			", templateCode=" + templateCode +
			", validateCode=" + validateCode +
			"}";
	}
}
