package com.alinesno.cloud.base.notice.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 07:19:08
 */
@SuppressWarnings("serial")
public class SmsSettingsDto extends BaseDto {

    /**
     * 设置代码
     */
	private String settingsCode;
	
    /**
     * 设置名称
     */
	private String settingsName;
	
    /**
     * 设置名称
     */
	private String settingsValue;
	
    /**
     * 设置过滤字段
     */
	private String settingsFilter;
	


	public String getSettingsCode() {
		return settingsCode;
	}

	public void setSettingsCode(String settingsCode) {
		this.settingsCode = settingsCode;
	}

	public String getSettingsName() {
		return settingsName;
	}

	public void setSettingsName(String settingsName) {
		this.settingsName = settingsName;
	}

	public String getSettingsValue() {
		return settingsValue;
	}

	public void setSettingsValue(String settingsValue) {
		this.settingsValue = settingsValue;
	}

	public String getSettingsFilter() {
		return settingsFilter;
	}

	public void setSettingsFilter(String settingsFilter) {
		this.settingsFilter = settingsFilter;
	}

}
