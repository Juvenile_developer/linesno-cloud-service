package com.alinesno.cloud.base.notice.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;
import java.util.Date;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 07:19:08
 */
@SuppressWarnings("serial")
public class EmailSendDto extends BaseDto {

	private String emailContent;
	
	private String emailReceiver;
	
	private Integer emailSendCount;
	
	private Date emailSendTime;
	
	private String emailSender;
	
	private String emailSubject;
	
	private String emailType;
	


	public String getEmailContent() {
		return emailContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	public String getEmailReceiver() {
		return emailReceiver;
	}

	public void setEmailReceiver(String emailReceiver) {
		this.emailReceiver = emailReceiver;
	}

	public Integer getEmailSendCount() {
		return emailSendCount;
	}

	public void setEmailSendCount(Integer emailSendCount) {
		this.emailSendCount = emailSendCount;
	}

	public Date getEmailSendTime() {
		return emailSendTime;
	}

	public void setEmailSendTime(Date emailSendTime) {
		this.emailSendTime = emailSendTime;
	}

	public String getEmailSender() {
		return emailSender;
	}

	public void setEmailSender(String emailSender) {
		this.emailSender = emailSender;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

}
