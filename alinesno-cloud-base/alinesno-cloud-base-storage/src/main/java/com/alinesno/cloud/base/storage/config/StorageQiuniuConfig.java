package com.alinesno.cloud.base.storage.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 七牛文件配置属性
 * @author LuoAnDong
 * @since 2019年4月10日 上午6:16:19
 */
@Component
@ConfigurationProperties(prefix = "storage.qiniu")
public class StorageQiuniuConfig {
	
	private String accessKey ; //: YJWlm_dypHR9UbXKcibpEzpDY8Ve_VbCLjhaG9Ne
    private String secretKey ; //: -zR3es1i54oIWNDqAPMo_PRhwetS7PK4ZoVI5_O9
    private String spaceBucket ; //: training-data
    private String spaceZone ; //: 2
    private String hostDomain ; //: http://data.linesno.com
    private String hostDomainDeadline ; //: 60
    
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public String getSpaceBucket() {
		return spaceBucket;
	}
	public void setSpaceBucket(String spaceBucket) {
		this.spaceBucket = spaceBucket;
	}
	public String getSpaceZone() {
		return spaceZone;
	}
	public void setSpaceZone(String spaceZone) {
		this.spaceZone = spaceZone;
	}
	public String getHostDomain() {
		return hostDomain;
	}
	public void setHostDomain(String hostDomain) {
		this.hostDomain = hostDomain;
	}
	public String getHostDomainDeadline() {
		return hostDomainDeadline;
	}
	public void setHostDomainDeadline(String hostDomainDeadline) {
		this.hostDomainDeadline = hostDomainDeadline;
	}
    
}
