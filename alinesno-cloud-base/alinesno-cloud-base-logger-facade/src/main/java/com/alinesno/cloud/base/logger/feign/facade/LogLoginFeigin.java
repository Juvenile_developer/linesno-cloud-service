package com.alinesno.cloud.base.logger.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;
import com.alinesno.cloud.base.logger.feign.dto.LogLoginDto;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@FeignClient(name="alinesno-cloud-base-logger" , path="logLogin")
public interface LogLoginFeigin extends IBaseFeign<LogLoginDto> {

}
