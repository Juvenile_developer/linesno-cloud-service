package com.alinesno.cloud.base.message.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;

import com.alinesno.cloud.base.message.feign.dto.TransactionMessageHistoryDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 09:28:52
 */
@FeignClient(name="alinesno-cloud-base-message" , path="transactionMessageHistory")
public interface TransactionMessageHistoryFeigin extends IBaseFeign<TransactionMessageHistoryDto> {

}
