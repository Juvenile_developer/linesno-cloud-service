package com.alinesno.cloud.base.print.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2019-05-03 14:07:38
 */
@Entity
@Table(name="template_version")
public class TemplateVersionEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 打印模板版本号
     */
	private String templateVersion;
    /**
     * 打印模板内容
     */
	private String printTemplate;
    /**
     * 编辑作者
     */
	private String author;


	public String getTemplateVersion() {
		return templateVersion;
	}

	public void setTemplateVersion(String templateVersion) {
		this.templateVersion = templateVersion;
	}

	public String getPrintTemplate() {
		return printTemplate;
	}

	public void setPrintTemplate(String printTemplate) {
		this.printTemplate = printTemplate;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}


	@Override
	public String toString() {
		return "TemplateVersionEntity{" +
			"templateVersion=" + templateVersion +
			", printTemplate=" + printTemplate +
			", author=" + author +
			"}";
	}
}
