package com.alinesno.cloud.common.facade.wrapper;

import java.io.Serializable;

/**
 * 条件基类
 * 
 * @author LuoAnDong
 * @since 2018年12月15日 上午8:32:20
 */
public abstract class Wrapper implements Serializable {

	private static final long serialVersionUID = 4005829114789471495L;

	private int pageNumber ; // 第几页
	private int pageSize ; // 每页显示 
	
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	

}
