package com.alinesno.cloud.common.web.login.shiro.redis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Shiro与Redis整合缓存
 * @param key
 * @return
 */
public class ShiroRedisCache<K,V> implements Cache<K,V> {

	private RedisTemplate<String, Object> redisTemplate;
    private String prefix = "shiro_redis_cache_";

    public String getPrefix() {
        return prefix+":";
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public ShiroRedisCache(RedisTemplate<String, Object> redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    public ShiroRedisCache(RedisTemplate<String, Object> redisTemplate,String prefix){
        this(redisTemplate);
        this.prefix = prefix;
    }

	@SuppressWarnings("unchecked")
	@Override
    public V get(K k) throws CacheException {
        if (k == null) {
            return null;
        }
        Object bytes = getBytesKey(k);
        return (V)redisTemplate.opsForValue().get(bytes);

    }

    @Override
    public V put(K k, V v) throws CacheException {
        if (k== null || v == null) {
            return null;
        }

        String bytes = getBytesKey(k);
        redisTemplate.opsForValue().set(bytes, v);
        return v;
    }

    @SuppressWarnings("unchecked")
	@Override
    public V remove(K k) throws CacheException {
    
        if(k==null){
            return null;
        }
        String bytes =getBytesKey(k);
        V v = (V)redisTemplate.opsForValue().get(bytes);
        redisTemplate.delete(bytes);
        return v;
    }

    @Override
    public void clear() throws CacheException {
        redisTemplate.getConnectionFactory().getConnection().flushDb();

    }

    @Override
    public int size() {
        return redisTemplate.getConnectionFactory().getConnection().dbSize().intValue();
    }

    @SuppressWarnings("unchecked")
	@Override
    public Set<K> keys() {
    	String bytes = (getPrefix()+"*");
        Set<String> keys = redisTemplate.keys(bytes);
        Set<K> sets = new HashSet<>();
        for (Object key:keys) {
            sets.add((K)key);
        }
        return sets;
    }

    @Override
    public Collection<V> values() {
        Set<K> keys = keys();
        List<V> values = new ArrayList<>(keys.size());
        for(K k :keys){
            values.add(get(k));
        }
        return values;
    }

    private String getBytesKey(K key){
        if(key instanceof String){
            String prekey = this.getPrefix() + key;
            return prekey;
        }else {
            return key+"" ;
        }
    }
    
   
}