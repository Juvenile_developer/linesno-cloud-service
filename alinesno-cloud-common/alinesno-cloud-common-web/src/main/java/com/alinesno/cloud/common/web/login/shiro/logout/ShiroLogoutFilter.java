package com.alinesno.cloud.common.web.login.shiro.logout;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.session.SessionException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * shiro自定义用户退出 
 * @author LuoAnDong
 * @since 2019年4月9日 上午8:09:33
 */
public class ShiroLogoutFilter extends LogoutFilter {
	
	private static final Logger log = LoggerFactory.getLogger(ShiroLogoutFilter.class);
	
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
    	
    	log.debug("ShiroLogoutFilter用户退出.");
    	
        //在这里执行退出系统前需要清空的数据
        Subject subject=getSubject(request,response);
        
        try {
            subject.logout();
        }catch (SessionException e){
            e.printStackTrace();
        }
        
        String redirectUrl=getRedirectUrl(request,response,subject);
        issueRedirect(request,response,redirectUrl);
        return false;
    }
}