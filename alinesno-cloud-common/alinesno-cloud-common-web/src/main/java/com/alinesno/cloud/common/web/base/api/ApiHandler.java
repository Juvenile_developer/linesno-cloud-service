package com.alinesno.cloud.common.web.base.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 微信接口实现
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:48:27
 */
@Scope("prototype")
@Controller
public class ApiHandler {

	private static final Logger log = LoggerFactory.getLogger(ApiHandler.class);

	public static final String REQUEST_METHOD_ERROR = "请求方式不正确.";
	public static final String REQUEST_VERSION_ERROR = "请求版本号不正确.";
	public static final String BUSINESS_NOT_USE = "业务处理不存在.";
	public static final String USER_NOT_LOGIN = "用户未登陆.";
	public static final String BUSINESS_SYSTEM_ERROR = "系统内部错误,请及时联系管理员";

	/**
	 * 处理请求业务
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/api/{version}/{model}/{method}", method = { RequestMethod.GET, RequestMethod.POST })
	public Object handler(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("version") String version, @PathVariable("model") String model,
			@PathVariable("method") String method) {

		String requestMethodType = request.getMethod(); // 请求方式
		log.debug("model:{} , method:{} , requestMethod:{}", model, method, requestMethodType);

		// try {
		// String beanName = "handle_"+version+"_"+model+"_"+method ;
		// logger.debug("spring bean name = {}" , beanName);
		// ApiParent handler = (ApiParent) SpringContext.getBean(beanName) ;
		// ApiAnnotation apiAnnotation =
		// handler.getClass().getAnnotation(ApiAnnotation.class) ;
		//
		// String apiDesc = apiAnnotation.desc() ;
		// String requestMethod = apiAnnotation.method().value() ;
		// boolean validate = apiAnnotation.validate() ;
		// ReturnType backType = apiAnnotation.back() ;
		//
		// if(validate) {
		// //判断是否需要验证权限
		// if(!isLogin()) {
		// return ResultGenerator.genFailResult(apiDesc + " , " +USER_NOT_LOGIN);
		// }
		// }
		//
		// //判断请求方式
		// if(!(ApiRequestMethod.HTTP.value()).equals(requestMethod) &&
		// !requestMethodType.equalsIgnoreCase(requestMethod)) {
		// return ResultGenerator.genFailResult(apiDesc + " , " +REQUEST_METHOD_ERROR);
		// }
		//
		// Object backObj = handler.handler(request, response) ;
		// if(backType == ReturnType.REDITRECT) {
		// response.sendRedirect((String)backObj);
		// return null ;
		// }
		// return backObj ;
		// }catch(NoSuchBeanDefinitionException | IOException e) {
		// logger.error("找不到服务类:{}" , e);
		// return ResultGenerator.genFailResult(BUSINESS_NOT_USE);
		// }

//		return ResultGenerator.genFailResult(BUSINESS_NOT_USE);

		return null;

	}

}
